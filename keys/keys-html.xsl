<?xml version="1.0" encoding="utf-8"?> 

<!DOCTYPE xsl:stylesheet [ 
  <!ENTITY nbsp "&#160;"> 
  <!ENTITY mdash "&#8212;">
]>

<xsl:stylesheet
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
   xmlns="http://www.w3.org/1999/xhtml"
>

<xsl:output
   method="xml"
   encoding="utf-8"
   indent="yes"
   doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
   doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
   omit-xml-declaration="yes"/>

<!-- page skeleton -->
<xsl:template match="book">
<xsl:comment><xsl:value-of select="/book/info/note"/></xsl:comment>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="./keys.css" media="screen" type="text/css" />
<title><xsl:value-of select="/book/info/title"/></title>
</head>

<body class="kmr">

<!-- body content -->
<div id="header">
<h1 class="kmr"><xsl:value-of select="/book/info/title"/></h1>
<div class="kmr-releaseinfo"><xsl:value-of select="/book/info/releaselabel"/>&#160;<xsl:value-of select="/book/info/releaseinfo"/></div>
<div class="kmr-pubdate"><xsl:value-of select="/book/info/datelabel"/>&#160;<xsl:value-of select="/book/info/pubdate"/></div>
</div>

<xsl:apply-templates select="preface"/>

<!-- table of content -->
<div class="kmr-toc">
<ul>
<xsl:for-each select="//sect1">
<li><a class="kmr-toc-section" href="#{generate-id()}"><xsl:value-of select="./title"/></a></li>
<xsl:if test="./sect2[title]">
<ul>
<xsl:for-each select="./sect2[title]">
<li><a class="kmr-toc-group" href="#{generate-id()}"><xsl:value-of select="./title"/></a></li>
</xsl:for-each>
</ul>
</xsl:if>
</xsl:for-each>
</ul>
</div>

<!-- Chapters -->
<div class="kmr-chapter">
<xsl:apply-templates select="chapter"/>
</div>

</body>
</html>
</xsl:template>

<!-- Other templates -->
<xsl:template match="preface">
<div id="kmr-preface"><xsl:apply-templates/></div>
</xsl:template>

<xsl:template match="para">
<p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="a">
<a href="{@href}"><xsl:apply-templates/></a>
</xsl:template>

<xsl:template match="title">
</xsl:template>

<xsl:template match="book/text() | chapter/text()"/>

<xsl:template match="chapter">
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="sect1">
<div class="kmr-sect">
<a name="{generate-id()}"/>
<h2 class="kmr"><xsl:value-of select="./title"/></h2>
<table class="kmr kmr-{@color}"><xsl:apply-templates/></table>
</div>
</xsl:template>

<xsl:template match="sect2/title">
<tr><td colspan="2"><h3 class="kmr"><a name="{generate-id(..)}"/><xsl:value-of select="text()"/></h3></td></tr>
</xsl:template>

<xsl:template match="note">
<tr><td colspan="2" class="kmr-note"><span><xsl:value-of select="text()"/></span></td></tr>
</xsl:template>

<xsl:template match="keys | mouse">
<tr>
<td class="kmr-keys">
<xsl:choose>
<xsl:when test="count(shortcut) = 2">
<span class="kmr-shortcut"><xsl:apply-templates select="shortcut[1]"/></span>,
<span class="kmr-shortcut"><xsl:apply-templates select="shortcut[2]"/></span>
</xsl:when>
<xsl:otherwise>
<span class="kmr-shortcut"><xsl:apply-templates select="shortcut[1]"/></span>
</xsl:otherwise>
</xsl:choose>
</td>
<td>
<span class="kmr-action">
<xsl:apply-templates select="action"/>
</span>
</td>
</tr>
</xsl:template>

<xsl:template match="action">
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="shortcut">
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="keycombo">
<xsl:choose>
<xsl:when test="count(keycap) = 4">
<xsl:apply-templates select="keycap[1]"/><span class="kmr-plussign">+</span><xsl:apply-templates select="keycap[2]"/><span class="kmr-plussign">+</span><xsl:apply-templates select="keycap[3]"/><span class="kmr-plussign">+</span><xsl:apply-templates select="keycap[4]"/>
</xsl:when>
<xsl:when test="count(keycap) = 3">
<xsl:apply-templates select="keycap[1]"/><span class="kmr-plussign">+</span><xsl:apply-templates select="keycap[2]"/><span class="kmr-plussign">+</span><xsl:apply-templates select="keycap[3]"/>
</xsl:when>
<xsl:when test="count(keycap) = 2">
<xsl:apply-templates select="keycap[1]"/><span class="kmr-plussign">+</span><xsl:apply-templates select="keycap[2]"/>
</xsl:when>
<xsl:otherwise>
<xsl:apply-templates select="keycap[1]"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="keycap">
<xsl:choose>
<xsl:when test="@type='pad'"><span class="kmr-keypad"><xsl:value-of select="text()"/></span></xsl:when>
<xsl:otherwise><xsl:value-of select="text()"/></xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>
